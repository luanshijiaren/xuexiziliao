/*begin
2015-03-16 11:31:31 By wd@5173.com*/
(function() {
    function h(a) {
        if (typeof a === "function") throw new Error("SwfStore Error: Functions cannot be used as keys or values.");
    }
    var b = 0,
    f = /[^a-z0-9_\/]/ig;
    window.SwfStore = function(a) {
        function e() {
            return "SwfStore_" + a.namespace.replace("/", "_") + "_" + b++
        }
        function c(j) {
            var i = document.createElement("div");
            document.body.appendChild(i);
            i.id = e();
            if (!j) {
                i.style.position = "absolute";
                i.style.top = "-2000px";
                i.style.left = "-2000px"
            }
            return i
        }
        a = a || {};
        var d = {
            swf_url: "storage.swf",
            namespace: "swfstore",
            debug: false,
            timeout: 10,
            onready: null,
            onerror: null
        },
        g;
        for (g in d) if (d.hasOwnProperty(g)) a.hasOwnProperty(g) || (a[g] = d[g]);
        a.namespace = a.namespace.replace(f, "_");
        if (window.SwfStore[a.namespace]) throw "There is already an instance of SwfStore using the '" + a.namespace + "' namespace. Use that instance or specify an alternate namespace in the config.";
        this.config = a;
        if (a.debug) {
            if (typeof console === "undefined") {
                var k = c(true);
                this.console = {
                    log: function(j) {
                        var i = c(true);
                        i.innerHTML = j;
                        k.appendChild(i)
                    }
                }
            } else this.console = console;
            this.log = function(j, i, m) {
                i = i === "swfStore" ? "swf": i;
                typeof this.console[j] !== "undefined" ? this.console[j]("SwfStore - " + a.namespace + " (" + i + "): " + m) : this.console.log("SwfStore - " + a.namespace + ": " + j + " (" + i + "): " + m)
            }
        } else this.log = function() {};
        this.log("info", "js", "Initializing...");
        SwfStore[a.namespace.replace("/", "_")] = this;
        d = c(a.debug);
        g = e();
        var l = "namespace=" + encodeURIComponent(a.namespace);
        d.innerHTML = '<object height="100" width="500" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="' + g + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">\t<param value="' + a.swf_url + '" name="movie">\t<param value="' + l + '" name="FlashVars">\t<param value="always" name="allowScriptAccess">\t<embed height="375" align="middle" width="500" pluginspage="https://www.macromedia.com/go/getflashplayer" flashvars="' + l + '" type="application/x-shockwave-flash" allowscriptaccess="always" quality="high" loop="false" play="true" name="' + g + '" bgcolor="#ffffff" src="' + a.swf_url + '"></object>';
        this.swf = document[g] || window[g];
        this._timeout = setTimeout(function() {
            SwfStore[a.namespace].log("error", "js", "Timeout reached, assuming " + a.swf_url + " failed to load and firing the onerror callback.");
            a.onerror && a.onerror()
        },
        a.timeout * 1E3)
    };
    SwfStore.prototype = {
        ready: false,
        set: function(a, e) {
            this._checkReady();
            h(a);
            h(e);
            e === null || typeof e == "undefined" ? this.swf.clear(a) : this.swf.set(a, e)
        },
        get: function(a) {
            this._checkReady();
            h(a);
            return this.swf.get(a)
        },
        getAll: function() {
            this._checkReady();
            for (var a = this.swf.getAll(), e = {},
            c = 0, d = a.length, g; c < d; c++) {
                g = a[c];
                e[g.key] = g.value
            }
            return e
        },
        clearAll: function() {
            var a = this.getAll();
            for (var e in a) a.hasOwnProperty(e) && this.clear(e)
        },
        clear: function(a) {
            this._checkReady();
            h(a);
            this.swf.clear(a)
        },
        _checkReady: function() {
            if (!this.ready) throw "SwfStore is not yet finished initializing. Pass a config.onready callback or wait until this.ready is true before trying to use a SwfStore instance.";
        },
        onload: function() {
            var a = this;
            setTimeout(function() {
                clearTimeout(a._timeout);
                a.ready = true;
                a.config.onready && a.config.onready()
            },
            0)
        },
        onerror: function() {
            clearTimeout(this._timeout);
            this.config.onerror && this.config.onerror()
        }
    }
})(); (function(h, b, f) {
    if (typeof module !== "undefined" && module.exports) module.exports = f();
    else if (typeof define === "function" && define.amd) define(f);
    else b[h] = f()
})("Fingerprint", this,
function() {
    var h = function(b) {
        var f, a;
        f = Array.prototype.forEach;
        a = Array.prototype.map;
        this.each = function(e, c, d) {
            if (e !== null) if (f && e.forEach === f) e.forEach(c, d);
            else if (e.length === +e.length) for (var g = 0,
            k = e.length; g < k; g++) {
                if (c.call(d, e[g], g, e) === {}) return
            } else for (g in e) if (e.hasOwnProperty(g)) if (c.call(d, e[g], g, e) === {}) return
        };
        this.map = function(e, c, d) {
            var g = [];
            if (e == null) return g;
            if (a && e.map === a) return e.map(c, d);
            this.each(e,
            function(k, l, j) {
                g[g.length] = c.call(d, k, l, j)
            });
            return g
        };
        if (typeof b == "object") {
            this.hasher = b.hasher;
            this.screen_resolution = b.screen_resolution;
            this.canvas = b.canvas;
            this.ie_activex = b.ie_activex
        } else if (typeof b == "function") this.hasher = b
    };
    h.prototype = {
        get: function() {
            var b = [];
            b.push(navigator.userAgent);
            b.push(navigator.language);
            b.push(screen.colorDepth);
            this.screen_resolution && typeof this.getScreenResolution() !== "undefined" && b.push(this.getScreenResolution().join("x"));
            b.push((new Date).getTimezoneOffset());
            b.push(this.hasSessionStorage());
            b.push(this.hasLocalStorage());
            b.push( !! window.indexedDB);
            document.body ? b.push(typeof document.body.addBehavior) : b.push("undefined");
            b.push(typeof window.openDatabase);
            b.push(navigator.cpuClass);
            b.push(navigator.platform);
            b.push(navigator.doNotTrack);
            b.push(this.getPluginsString());
            this.canvas && this.isCanvasSupported() && b.push(this.getCanvasFingerprint());
            return this.hasher ? this.hasher(b.join("###"), 31) : this.murmurhash3_32_gc(b.join("###"), 31)
        },
        murmurhash3_32_gc: function(b, f) {
            var a, e, c, d;
            a = b.length & 3;
            e = b.length - a;
            c = f;
            for (f = 0; f < e;) {
                d = b.charCodeAt(f) & 255 | (b.charCodeAt(++f) & 255) << 8 | (b.charCodeAt(++f) & 255) << 16 | (b.charCodeAt(++f) & 255) << 24; ++f;
                d = (d & 65535) * 3432918353 + (((d >>> 16) * 3432918353 & 65535) << 16) & 4294967295;
                d = d << 15 | d >>> 17;
                d = (d & 65535) * 461845907 + (((d >>> 16) * 461845907 & 65535) << 16) & 4294967295;
                c ^= d;
                c = c << 13 | c >>> 19;
                c = (c & 65535) * 5 + (((c >>> 16) * 5 & 65535) << 16) & 4294967295;
                c = (c & 65535) + 27492 + (((c >>> 16) + 58964 & 65535) << 16)
            }
            d = 0;
            switch (a) {
            case 3:
                d ^= (b.charCodeAt(f + 2) & 255) << 16;
            case 2:
                d ^= (b.charCodeAt(f + 1) & 255) << 8;
            case 1:
                d ^= b.charCodeAt(f) & 255;
                d = (d & 65535) * 3432918353 + (((d >>> 16) * 3432918353 & 65535) << 16) & 4294967295;
                d = d << 15 | d >>> 17;
                d = (d & 65535) * 461845907 + (((d >>> 16) * 461845907 & 65535) << 16) & 4294967295;
                c ^= d
            }
            c ^= b.length;
            c ^= c >>> 16;
            c = (c & 65535) * 2246822507 + (((c >>> 16) * 2246822507 & 65535) << 16) & 4294967295;
            c ^= c >>> 13;
            c = (c & 65535) * 3266489909 + (((c >>> 16) * 3266489909 & 65535) << 16) & 4294967295;
            c ^= c >>> 16;
            return c >>> 0
        },
        hasLocalStorage: function() {
            try {
                return !! window.localStorage
            } catch(b) {
                return true
            }
        },
        hasSessionStorage: function() {
            try {
                return !! window.sessionStorage
            } catch(b) {
                return true
            }
        },
        isCanvasSupported: function() {
            var b = document.createElement("canvas");
            return !! (b.getContext && b.getContext("2d"))
        },
        isIE: function() {
            if (navigator.appName === "Microsoft Internet Explorer") return true;
            else if (navigator.appName === "Netscape" && /Trident/.test(navigator.userAgent)) return true;
            return false
        },
        getPluginsString: function() {
            return this.isIE() && this.ie_activex ? this.getIEPluginsString() : this.getRegularPluginsString()
        },
        getRegularPluginsString: function() {
            return this.map(navigator.plugins,
            function(b) {
                var f = this.map(b,
                function(a) {
                    return [a.type, a.suffixes].join("~")
                }).join(",");
                return [b.name, b.description, f].join("::")
            },
            this).join(";")
        },
        getIEPluginsString: function() {
            return window.ActiveXObject ? this.map(["ShockwaveFlash.ShockwaveFlash", "AcroPDF.PDF", "PDF.PdfCtrl", "QuickTime.QuickTime", "rmocx.RealPlayer G2 Control", "rmocx.RealPlayer G2 Control.1", "RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)", "RealVideo.RealVideo(tm) ActiveX Control (32-bit)", "RealPlayer", "SWCtl.SWCtl", "WMPlayer.OCX", "AgControl.AgControl", "Skype.Detection"],
            function(b) {
                try {
                    new ActiveXObject(b);
                    return b
                } catch(f) {
                    return null
                }
            }).join(";") : ""
        },
        getScreenResolution: function() {
            return [screen.height, screen.width]
        },
        getCanvasFingerprint: function() {
            var b = document.createElement("canvas"),
            f = b.getContext("2d");
            f.textBaseline = "top";
            f.font = "14px 'Arial'";
            f.textBaseline = "alphabetic";
            f.fillStyle = "#f60";
            f.fillRect(125, 1, 62, 20);
            f.fillStyle = "#069";
            f.fillText("http://www.5173.com", 2, 15);
            f.fillStyle = "rgba(102, 204, 0, 0.7)";
            f.fillText("http://www.5173.com", 4, 17);
            return b.toDataURL()
        }
    };
    return h
});
function getExplorer() {
    var h = window.navigator.userAgent;
    h = window.navigator.userAgent.indexOf("Chrome") !== -1;
    return $.browser.msie ? "IE " + $.browser.version: $.browser.mozilla ? "Firefox " + $.browser.version: $.browser.opera ? "Opera " + $.browser.version: h ? "Chrome": $.browser.safari ? "Safari " + $.browser.version: "Chrome\u6216\u5176\u4ed6\u6d4f\u89c8\u5668"
}
function getFP() {
    for (var h = "",
    b = "",
    f = 0; f < 33; f++) h += Math.floor(Math.random() * 10);
    var a = new SwfStore({
        namespace: "flash_cookie",
        swf_url: "https://images002.5173cdn.com/swf/flash_cookie.swf",
        debug: false,
        onready: function() {
            a.get("flashcookieval") == null && a.set("flashcookieval", h);
            b = a.get("flashcookieval")
        },
        onerror: function() {}
    });
    setTimeout(function() {
        var e = new Fingerprint,
        c = new Fingerprint({
            canvas: true
        }),
        d = new Fingerprint({
            ie_activex: true
        }),
        g = new Fingerprint({
            screen_resolution: true
        });
        e = e.get() + "|" + c.get() + "|" + d.get() + "|" + g.get();
        c = getExplorer();
        document.cookie = "fingerid=" + escape('[{"fingerid":"' + e + '"},{"browser":"' + c + '"},{"flashcookieval":"' + b + '"}]')
    },
    1E3)
};