/*begin
2014-05-20 10:42:12 By wd@5173.com*/
(function(b) {
    b.fost = b.extend(b.fost, {});
    b.fost.security = b.extend(b.fost.security, {});
    b.fost.security.updateBroker = function() {};
    b.fost.security.showBroker = function() {};
    b(function() {
        var i = "";
        i || (i = "securityContainer");
        b.fost.security.containerClientId = i;
        b.fost.security.valueClientId = "__validationValue__";
        b.fost.security.tokenClientId = "__validationToken__";
        b("#" + i).append("");
        String.prototype.format = function() {
            var a = arguments;
            return this.replace(/{(\d+)}/g,
            function(c, f) {
                return a[f]
            })
        };
        b.fost.security.showBroker = function(a) {
            a = b.extend({
                containerId: i
            },
            a);
            b.fost.security.FostTpm.showBroker(a)
        };
        b.fost.ieHelper = {
            saveUserData: function(a, c, f, d, g) {
                try {
                    var e = new Date;
                    e.setTime(e.getTime() + g);
                    var h = e.toUTCString(),
                    k = document.getElementById(a);
                    k.expires = h;
                    k.setAttribute(f, d);
                    k.save(c)
                } catch(l) {
                    return false
                }
                return true
            },
            getAndRemoveUserData: function(a, c, f) {
                try {
                    var d = document.getElementById(a);
                    d.load(c);
                    var g = d.getAttribute(f);
                    d.setAttribute(f, "");
                    d.removeAttribute(f);
                    d.save(c);
                    return g
                } catch(e) {}
            }
        };
        b.fost.security.FostTpm = b.extend(b.fost.security.FostTpm, {
            initilized: false,
            ready: false,
            installed: false,
            installedVersion: null,
            usingTpm: true,
            activeTpm: null,
            version: "1.0.22.0",
            codebase: "https://security.5173.com/Contents/Tpm/1.0.22.0/5173Tpm.cab#version=1,0,22,0",
            installFile: "https://security.5173.com/Contents/Tpm/1.0.22.0/5173Tpm.exe",
            installFile64: "https://security.5173.com/Contents/Tpm/1.0.22.0/5173Tpm.exe",
            error: false,
            width: 150,
            height: 25,
            secureTipId: "__secureTips__",
            secureControlId: "__secureEdit__",
            passwordRequired: true,
            passwordControlId: "password",
            safeSwitchControlId: "loginSwitcher",
            installByUser: false,
            installRequired: true,
            installDemand: true,
            installHtml: '<img id="{0}" style="cursor: pointer" src="https://security.5173.com/Contents/Images/tips150_blank.gif" border="0" width="{1}" height="{2}" alt="\u8bf7\u70b9\u6b64\u8f93\u5165\u5bc6\u7801" />',
            tmpHtml: null,
            fingerId: "__validationDna__",
            secureContainer: null,
            passwordOnly: false,
            passwordHash: false,
            passwordFill: true,
            tokenClientId: "__validationToken__",
            valueClientId: "__validationValue__",
            stoken: "53c546f98d8b447caa3097635356ccdd",
            svalue: "8888",
            ekey: "okwxty",
            vkey: "pi0qp0",
            pkey: "b3q4p1",
            getPassword: function(a) {
                if (this.ready && this.activeTpm && this.usingTpm) {
                    var c = this.activeTpm;
                    if (a) c.Seed = a;
                    return c.Password
                } else {
                    if ((c = b("#" + this.passwordControlId).val()) && this.passwordHash) if (typeof hex_md5 == "function") c = hex_md5(hex_md5(c).substr(8, 16) + a);
                    return c
                }
            },
            getDna: function(a, c, f, d, g) {
                if (this.ready && this.activeTpm) {
                    var e = this.activeTpm.UserIdentity;
                    if (e) {
                        if (a) e.FingerType = a;
                        if (typeof c != "undefined") e.HashMode = c;
                        if (f) e.CryptoMode = f;
                        if (d) e.ValidateKey = d;
                        if (g) e.EncryptKey = g;
                        return e.Identity
                    }
                    return e.Identity
                }
                return ""
            },
            showInstall: function(a) {
                if (this.secureContainer) if (this.installRequired || a) {
                    a = this.installHtml.format(this.secureTipId, this.width, this.height);
                    if (b.browser.msie && this.tpmHtml) a += this.tpmHtml;
                    this.secureContainer.html(a).show();
                    this.secureContainer.css({
                        position: "",
                        left: "",
                        visibility: ""
                    });
                    b("#" + this.secureTipId).click(function() {
                        b.fost.security.FostTpm.installHandler();
                        return false
                    });
                    this.passwordControlId && b("#" + this.passwordControlId).hide();
                    b("#" + this.secureControlId).hide();
                    this.installHandler()
                } else this.secureContainer.empty()
            },
            installHandler: function() {
                if (!this.dialogReady) {
                    b(document.body).append('\t\t\t<div id="layer_safe_before" class="UED_hide">\t\t\t\t<div class="pop_box">\t\t\t\t\t<div class="pop_tittle">\t\t\t\t\t  <h3>\u5b89\u5168\u63a7\u4ef6\u63d0\u793a</h3>\t\t\t\t\t  <a href="javascript:void(0)" class="close"></a>\t\t\t\t\t</div>\t\t\t\t\t<div class="pop_mainbox">\t\t\t\t\t\t<div class="side_icon pop_tips_1"></div>\t\t\t\t\t\t<div class="main_content">\t\t\t\t\t\t\t<p id="__new_safe_tip__" class="l_tt">\u60a8\u5c1a\u672a\u5b89\u88c5\u5bc6\u7801\u5b89\u5168\u63a7\u4ef6\uff0c\u5b89\u88c5\u540e\u5373\u53ef\u8f93\u5165\u5bc6\u7801\u3002</p>                            <p id="__upgrade_safe_tip__" style="display:none" class="l_tt">\u60a8\u9700\u8981\u5347\u7ea7\u5bc6\u7801\u5b89\u5168\u63a7\u4ef6\uff0c\u5347\u7ea7\u540e\u5373\u53ef\u8f93\u5165\u5bc6\u7801\u3002</p>\t\t\t\t\t\t\t<p>\u5b89\u5168\u63a7\u4ef6\u53ef\u5bf9\u60a8\u8f93\u5165\u7684\u5bc6\u7801\u8fdb\u884c\u52a0\u5bc6\u4fdd\u62a4\uff0c\u63d0\u5347\u8d26\u6237\u5b89\u5168\u6027\u3002<a href="http://aid.5173.com/zw_and_aq/dl/299.html" target="_blank">\u76f8\u5173\u5e2e\u52a9</a></p>\t\t\t\t\t\t\t<p class="r_txt mt10"><input type="button" value="\u5b89\u88c5\u63a7\u4ef6" id="btnInstall" /></p>\t\t\t\t\t\t</div>\t\t\t\t\t</div>\t\t\t\t</div>\t\t\t</div>\t\t\t<div id="layer_safe_after" class="UED_hide">\t\t\t\t<div class="pop_box">\t\t\t\t\t<div class="pop_tittle">\t\t\t\t\t  <h3>\u5b89\u5168\u63a7\u4ef6\u63d0\u793a</h3>\t\t\t\t\t  <a href="javascript:void(0)" class="close"></a>\t\t\t\t\t</div>\t\t\t\t\t<div class="pop_mainbox">\t\t\t\t\t\t<div class="side_icon pop_tips_1"></div>\t\t\t\t\t\t<div class="main_content">\t\t\t\t\t\t\t<p>\u5982\u679c\u60a8\u5df2\u4e0b\u8f7d\u63a7\u4ef6\u5b89\u88c5\u6587\u4ef6\uff0c\u53ef\u8fd0\u884c\u8be5\u6587\u4ef6\u5b8c\u6210\u5b89\u88c5\uff0c<span class="orange">\u5b89\u88c5\u6210\u529f\u540e\uff0c\u8bf7\u70b9\u51fb\u201c\u5237\u65b0\u6d4f\u89c8\u5668\u201d\u6216\u5173\u95ed\u5f53\u524d\u6d4f\u89c8\u5668\u91cd\u65b0\u6253\u5f00\u3002</span></p>\t\t\t\t\t\t\t<p class="r_txt mt10"><input type="button" value="\u5237\u65b0\u6d4f\u89c8\u5668" id="btnRefreshBrowser" /><input type="button" value="\u91cd\u65b0\u4e0b\u8f7d" id="btnReInstall" /></p>\t\t\t\t\t\t</div>\t\t\t\t\t</div>\t\t\t\t</div>\t\t\t</div>\t\t\t');
                    var a = function() {
                        window.open(b.fost.security.FostTpm.selectedFile, "_self");
                        b("#layer_safe_before").hide();
                        b.LAYER.show({
                            id: "layer_safe_after",
                            layerContainer: "mark",
                            overlay: {
                                color: "#000",
                                opacity: 0.5
                            }
                        })
                    };
                    b("#btnInstall").click(function() {
                        b.fost.security.FostTpm.selectedFile = b.fost.security.FostTpm.installFile;
                        a();
                        return false
                    });
                    b("#btnInstall64").click(function() {
                        b.fost.security.FostTpm.selectedFile = b.fost.security.FostTpm.installFile64;
                        a();
                        return false
                    });
                    b("#btnReInstall").click(function() {
                        a();
                        return false
                    });
                    b("#btnRefreshBrowser").click(function() {
                        window.location.reload(true);
                        return false
                    });
                    b("a.close").click(function() {
                        b.fost.security.FostTpm.restoreBroker();
                        b.LAYER.close()
                    });
                    this.dialogReady = true
                }
                b.LAYER.show({
                    id: "layer_safe_before",
                    layerContainer: "mark",
                    overlay: {
                        color: "#000",
                        opacity: 0.5
                    }
                });
                if (b.fost.security.FostTpm.installedVersion < b.fost.security.FostTpm.version) {
                    b("#__new_safe_tip__").hide();
                    b("#__upgrade_safe_tip__").show()
                }
            },
            readyHandler: function(a) {
                if (a.readyState == 4) if (a.Version && a.Version >= this.version && !this.ready) {
                    b("#" + this.secureTipId).hide();
                    this.showHandler(a);
                    try {
                        b.LAYER.close()
                    } catch(c) {}
                } else ! this.installRequired && !this.installByUser && this.restoreBroker()
            },
            errorHandler: function() {
                this.error = true
            },
            onsubmit: null,
            onready: null,
            onkeydown: function(a) {
                if (a.keyCode == 13) if (a = b.fost.security.FostTpm.secureContainer) if (a = a.parents("form")) {
                    a.submit();
                    return false
                }
            },
            submitHandler: function() {
                var a = b.fost.security.FostTpm,
                c = a.getDna(null, false, null, a.vkey, a.ekey);
                b("#" + a.fingerId).val(c);
                if (a.usingTpm || a.passwordHash) {
                    c = a.getPassword(a.pkey);
                    a.passwordFill && b("#" + a.passwordControlId).val(c)
                } else c = b("#" + a.passwordControlId).val();
                a.passwordOnly || b("#" + a.valueClientId).val(c);
                return typeof a.onsubmit == "function" ? a.onsubmit(a, c) : true
            },
            showHandler: function(a) {
                if (a) this.activeTpm = a;
                else a = this.activeTpm;
                this.ready = true;
                b("#" + this.secureTipId).hide();
                b(a).css("visibility", "");
                b(a).show();
                b("#" + this.passwordControlId).hide();
                this.safeSwitchControlId && b("#" + this.safeSwitchControlId).attr("disabled", "").attr("checked", "checked");
                if (this.passwordRequired) {
                    this.secureContainer.show();
                    this.secureContainer.css({
                        position: "",
                        left: "",
                        visibility: ""
                    })
                } else b.browser.msie ? this.secureContainer.hide() : this.secureContainer.css({
                    position: "absolute",
                    left: "-1000px",
                    visibility: "hidden"
                });
                this.usingTpm = true;
                typeof this.onready == "function" && this.onready(this)
            },
            showBroker: function(a) {
                if (this.initilized) this.ready ? this.showHandler() : this.showInstall(true);
                else {
                    this.initilized = true;
                    this.initBroker(a)
                }
            },
            initBroker: function(a) {
                a = b.extend({
                    installDemand: false,
                    passwordOnly: true
                },
                a);
                a = b.extend({
                    passwordFill: true,
                    passwordRequired: true,
                    passwordControlId: "password",
                    width: this.width,
                    height: this.height,
                    installRequired: false,
                    installDemand: true,
                    installHtml: this.installHtml,
                    safeSwitchControlId: "loginSwitcher",
                    onkeydown: this.onkeydown
                },
                a);
                this.width = a.width;
                this.height = a.height;
                this.passwordRequired = a.passwordRequired;
                this.passwordControlId = a.passwordControlId;
                this.safeSwitchControlId = a.safeSwitchControlId;
                this.installRequired = a.installRequired;
                this.installDemand = a.installDemand;
                this.installHtml = a.installHtml;
                this.onsubmit = a.onsubmit;
                this.onready = a.onready;
                this.onkeydown = a.onkeydown;
                this.passwordOnly = a.passwordOnly;
                this.passwordHash = a.passwordHash;
                this.passwordFill = a.passwordFill;
                this.stoken = a.stoken;
                this.svalue = a.svalue;
                this.ekey = a.ekey;
                this.vkey = a.vkey;
                this.pkey = a.pkey;
                this.safeSwitchControlId && b("#" + this.safeSwitchControlId).attr("checked", "").click(function() {
                    var c = b.fost.security.FostTpm;
                    if (this.checked || c.safeSwitchControlOnChecking) {
                        var f = c.safeSwitchControlOnChecking;
                        c.safeSwitchControlOnChecking = false;
                        c.installByUser = true;
                        c.showBroker();
                        b.browser.msie && !c.installed && !f && b.fost.ieHelper.saveUserData(c.ieUserData, "sTimeout", "installByUser", "true", 3E4)
                    } else c.restoreBroker()
                });
                if (this.show(a) == false) this.showInstall();
                else this.passwordControlId && b("#" + this.passwordControlId).hide();
                if (this.secureContainer)(a = this.secureContainer.parents("form")) && a.submit(this.submitHandler);
                if (b.browser.msie && !this.installed) {
                    this.ieUserData = "__secureUserData__";
                    b(document.body).append('<span id="{0}" style="display:none; behavior:url(\'#default#userData\')" />'.format(this.ieUserData));
                    if (b.fost.ieHelper.getAndRemoveUserData(this.ieUserData, "sTimeout", "installByUser") === "true") {
                        this.safeSwitchControlOnChecking = true;
                        b("#" + this.safeSwitchControlId).click()
                    }
                }
            },
            show: function(a) {
                var c = this.version,
                f = this.codebase,
                d = null,
                g = null,
                e = false,
                h = b.browser.msie;
                if (h) try {
                    d = new ActiveXObject("FostTpm.SecureEdit");
                    g = d.Version;
                    e = true
                } catch(k) {} else if (d = navigator.mimeTypes["application/x-fosttpm"]) {
                    d = d.enabledPlugin;
                    g = d.description;
                    if (g == "5173 Security Control") g = "1.0.21.0";
                    e = true
                }
                d = null;
                this.installed = e;
                this.installedVersion = g; (d = a.containerId) || (d = "secureContainer");
                d = this.secureContainer = b("#" + d);
                a = this.secureControlId;
                var l = this.secureTipId,
                r = this.fingerId,
                n = this.width,
                o = this.height,
                m = '<object style="visibility:hidden" width="{0}" height="{1}" id="{2}"{3} type="application/x-fosttpm" codebase="{4}" tabindex="2"><param name="SoftKeyboard" value="random"/><param name="AllowedDomains"  value=".5173.com,20200516,CmGZlhzh4UAheMQiC+F4kvWaQfNMpw3cORKZiiapRTfvuI88P8Qirr3B4nV8MbRWxQoyhVLZvtbWjmoFyr4+uUqI1keBlmWigGEys1qL/L9OyfsQqesAZnYRWfdYxpGVcBuZWgXDPO7QT2bo9ZIufngPrJcQblDTwtKjM9YPdTo=" /></object>',
                j = '\t\t\t\t<span {1}>\t\t\t\t\t<input id="' + this.tokenClientId + '" name="' + this.tokenClientId + '" type="hidden" value="' + this.stoken + '" />\t\t\t';
                this.passwordOnly || (j += '\t\t\t\t\t<input id="' + this.valueClientId + '" name="' + this.valueClientId + '" type="hidden" />\t\t\t\t');
                j += '\t\t\t\t\t<input id="{0}" name="{0}" type="hidden" />\t\t\t\t</span>\t\t\t';
                var p = "",
                q = 'style="position:absolute; left: -1000px; visibility: hidden;"';
                if (h) {
                    p = ' classid="CLSID:D05739E4-544C-412F-91C2-D28F95C0A1E0" onerror="javascript:$.fost.security.FostTpm.errorHandler(this);" onreadystatechange="javascript:$.fost.security.FostTpm.readyHandler(this);"';
                    q = 'style="display: none"'
                }
                m = m.format(n, o, a, p, f);
                j = j.format(r, q);
                tpmHtml = this.tpmHtml = m;
                if (this.installRequired || this.installDemand) tpmHtml = this.installHtml.format(l, n, o) + tpmHtml;
                d.parents("form").append(j);
                if (!h || !this.installDemand) if (e) {
                    if (g && c > g) return false
                } else return false;
                d.html(tpmHtml);
                b("#" + l).click(function() {
                    b.fost.security.FostTpm.installHandler();
                    return false
                });
                d = document.getElementById(a);
                if (!d) return h ? -1 : false;
                f = d.Version;
                g = b(d);
                if (h && !f) {
                    g.hide();
                    return - 1
                } else if (f < c) return this.ready = false;
                g.keydown(this.onkeydown);
                this.showHandler(d);
                return true
            },
            restoreBroker: function() {
                this.usingTpm = false;
                var a = b.browser.msie;
                if (this.secureContainer) a ? this.secureContainer.hide() : this.secureContainer.css({
                    position: "absolute",
                    left: "-1000px",
                    visibility: "hidden"
                });
                b("#" + this.secureTipId).hide();
                var c = this.activeTpm;
                c && a && b(c).hide();
                b("#" + this.passwordControlId).show();
                b("#" + this.safeSwitchControlId).attr("checked", "")
            }
        })
    })
})(jQuery);