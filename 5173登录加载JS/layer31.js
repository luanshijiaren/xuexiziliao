/*begin*/
var UED = {
    layer31: {
        openLayerID: "",
        thisID: "",
        currentlyLayerID: "",
        layerTitle: "",
        layerTitleID: "UED_TITLE_ID",
        layerContainer: "UED_lightbox",
        layerClose: 2,
        noframe: 0,
        init: function() {
            if (!document.getElementById("UED_iframe_overlay_v31")) {
                var _if = document.createElement("iframe");
                _if.id = "UED_iframe_overlay_v31";
                _if.style.dispay = "none";
                _if.frameborder = "0";
                _if.src = "javascript:false;";
                document.body.appendChild(_if)
            }
            if (!document.getElementById("UED_overlay_v31")) {
                var _bg = document.createElement("div");
                _bg.id = "UED_overlay_v31";
                document.body.appendChild(_bg)
            }
        },
        show: function(json) {
            this.init();
            this.thisID = json.id;
            this.noframe = json.noframe;
            if (this.openLayerID) {
                document.getElementById(this.openLayerID).style.display = "none";
                this.openLayerID = ""
            }
            if (json.layerContainer) this.layerContainer = json.layerContainer;
            else this.layerContainer = "UED_lightbox";
            if (json.close == 0) this.layerClose = 0;
            else this.layerClose = 2;
            var _ul = document.getElementById(this.layerContainer).style;
            var _me = document.getElementById(json.id).style;
            document.getElementById(json.id).style.display = "block";
            _ul.display = "block";
            _ul.height = json.height + 31 + "px";
            _ul.width = json.width + "px";
            _ul.marginLeft = -1 * (json.width / 2) + "px";
            _me.width = json.width - 2 + "px";
            _me.height = json.height - 4 + "px";
            if (window.XMLHttpRequest) _ul.marginTop = -1 * (json.height / 2) + "px";
            this.h2title(json.title);
            document.getElementById("UED_iframe_overlay_v31").style.display = "block";
            document.getElementById("UED_overlay_v31").style.display = "block";
            this.openLayerID = json.id
        },
        close: function(isWin) {
            document.getElementById(this.openLayerID).style.display = "none";
            document.getElementById(this.layerContainer).style.display = "none";
            document.getElementById("UED_iframe_overlay_v31").style.display = "none";
            document.getElementById("UED_overlay_v31").style.display = "none";
            if (isWin === 1) try {
                if (typeof call_back_win_js_alert_close() == "function") call_back_win_js_alert_close()
            } catch(e) {} else try {
                if (typeof call_back_js_alert_close() == "function") call_back_js_alert_close()
            } catch(e) {}
        },
        h2title: function(title) {
            if (UED.layer31.noframe == 1) return;
            var _title = document.createElement("div");
            _title.id = this.layerTitleID;
            if (this.layerClose == 0) _tt = '<h3 id="UED_layer_h3_v31"><span id="UED_layer_title_v31">' + title + "</span></h3>";
            else _tt = '<h3 id="UED_layer_h3_v31"><a id="UED_layer_title_close_v31" class="UED_close_lightbox_v31" href="javascript:UED.layer31.close(1)"></a><span id="UED_layer_title_v31">' + title + "</span></h3>";
            _title.innerHTML = _tt;
            var _ul = document.getElementById(this.layerContainer);
            if (_ul.getElementsByTagName("div")[0].id == this.layerTitleID) _ul.getElementsByTagName("div")[0].innerHTML = _tt;
            else _ul.insertBefore(_title, _ul.childNodes[0])
        }
    }
};

/*end*/
