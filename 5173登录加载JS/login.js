/*begin
2016-07-05 15:35:10 By wd@5173.com*/
$.Capcha = $.extend($.Capcha, {
    capchaContainerId: "",
    capchaClientId: "",
    capchaImgId: "",
    needCapcha: false,
    capchaSrc: "",
    token: "",
    isValidate: false,
    validateMessage: "",
    captchaCreated: false,
    updateCapcha: function() {
        $("#" + this.capchaClientId).val("");
        $("#" + this.capchaImgId).attr("src", this.capchaSrc + "?t=" + this.token + "&r=" + Math.random());
        $("#yanzheng").removeClass("yanzheng_error");
        $("#yanzheng").removeClass("yanzheng_ok")
    },
    initCapcha: function(b) {
        b = $.extend({
            capchaContainerId: "capchaContainer",
            capchaClientId: "txtCode",
            capchaImgId: "__Captcha_Image__"
        },
        b);
        this.capchaContainerId = b.capchaContainerId;
        this.capchaClientId = b.capchaClientId;
        this.capchaImgId = b.capchaImgId;
        this.needCapcha = b.needCapcha;
        this.capchaSrc = b.capchaSrc;
        this.token = b.token;
        $("#txtCode__Captcha").click(function() {
            $.Capcha.updateCapcha();
            $("#" + $.Capcha.capchaClientId).focus();
            $("#loginTips").html("").hide()
        })
    },
    createCaptcha: function() {
        if (!this.captchaCreated) {
            if (this.needCapcha) {
                $("#" + this.capchaContainerId).css("display", "block");
                $.Capcha.updateCapcha()
            } else $("#" + this.capchaContainerId).css("display", "none");
            this.captchaCreated = true
        }
    },
    validateCapchaClient: function() {
        if (this.needCapcha) {
            if ($("#" + this.capchaClientId).val().length < 4) return "\u8bf7\u8f93\u5165\u6b63\u786e\u7684\u9a8c\u8bc1\u7801";
            if (!this.isValidate) return this.validateMessage
        }
        return ""
    },
    CheckValidateCapcha: function() {
        if (this.needCapcha) if ($("#" + this.capchaClientId).val().length == 4) $.ajax({
            type: "post",
            url: "/SSO/ValidateCaptcha",
            data: {
                code: $("#" + this.capchaClientId).val(),
                token: $.Capcha.token
            },
            dataType: "json",
            success: function(b) {
                if (b.result == 0) {
                    $("#loginTips").html("").hide();
                    $.Capcha.isValidate = true;
                    $("#yanzheng").removeClass("yanzheng_error");
                    $("#yanzheng").addClass("yanzheng_ok")
                } else {
                    $.Capcha.isValidate = false;
                    $.Capcha.validateMessage = b.Message;
                    $("#yanzheng").removeClass("yanzheng_ok");
                    $("#yanzheng").addClass("yanzheng_error")
                }
            },
            error: function() {}
        });
        else {
            $.Capcha.isValidate = false;
            $.Capcha.validateMessage = "\u8bf7\u8f93\u5165\u6b63\u786e\u7684\u9a8c\u8bc1\u7801";
            $("#yanzheng").removeClass("yanzheng_ok");
            $("#yanzheng").addClass("yanzheng_error")
        }
        return $.Capcha.isValidate
    }
});
$(function() {
    var b = decodeURIComponent(getQueryStringByName("returnUrl"));
    if (b == "http://ztc.5173.com/" || b == "http://ztc.5173.com") $(".login-frame").css({
        "background-image": "url('https://images002.5173cdn.com/images/login/v1/1m/login_bg4.jpg')"
    })
});
function getQueryStringByName(b) {
    b = location.search.match(new RegExp("[?&]" + b + "=([^&]+)", "i"));
    if (b == null || b.length < 1) return "";
    return b[1]
}
var submitting = false;
function showTips(b, f) {
    $("#loginTips").html(b).show();
    if (f) {
        $("#btnSubmit").attr("disabled", "");
        $("#asp").attr("class", "btnlink_b_small");
        $("#asp_text").html("\u767b \u5f55");
        $("#asp_ico").hide();
        submitting = false
    }
}
function getCookie(b) {
    for (var f = document.cookie.split("; "), g = 0; g < f.length; g++) {
        var h = f[g].split("=");
        if (h[0] == b) return h[1]
    }
    return ""
}
$(function() {
    var b = $("#loginTips"),
    f = $("#loginForm"),
    g = $("#btnSubmit"),
    h = $("#asp"),
    l = $("#agreement"),
    j = {
        passwordControlId: "txtPass",
        passwordHash: true,
        passwordFill: false,
        width: 268,
        height: 34,
        stoken: $.fost.LoginParams.SecurityToken,
        ekey: $.fost.LoginParams.EncryptionKey,
        vkey: $.fost.LoginParams.ValidationKey,
        pkey: $.fost.LoginParams.PasswordKey,
        onsubmit: function(a, d) {
            a = decodeURIComponent(getCookie("fingerid"));
            $("#fp").val(a);
            if (submitting) return false;
            a = $("#txtName");
            if (!a.val()) {
                b.html("\u8bf7\u8f93\u5165\u7528\u6237\u540d").show();
                a.focus();
                return false
            }
            $.cookie("userName", $("#txtName").val());
            a = $("#txtPass");
            if (!d) {
                b.html("\u8bf7\u8f93\u5165\u5bc6\u7801").show();
                a.focus();
                return false
            }
            if (!l.attr("checked")) {
                b.html("\u5f88\u62b1\u6b49\uff0c\u8bf7\u9605\u8bfb\u5e76\u540c\u610f5173\u7528\u6237\u670d\u52a1\u534f\u8bae\u3002").show();
                l.focus();
                return false
            }
            if (!$("#pospod").is(":hidden")) {
                var e = $("#txtPodPaw");
                if (!e.val()) {
                    b.html("\u8bf7\u8f93\u5165\u5bc6\u5b9d").show();
                    e.focus();
                    return false
                }
            }
            e = $.Capcha.validateCapchaClient();
            if (e != "") {
                b.html(e).show();
                return false
            }
            b.html("").hide();
            a.val(d);
            g.attr("disabled", "disabled");
            h.attr("class", "btnlink_g_small unhover c9");
            $("#asp_text").html("\u52a0\u8f7d\u4e2d");
            $("#asp_ico").show();
            submitting = true;
            $.ajax({
                type: "POST",
                url: f.action,
                data: f.serialize(),
                success: function(c) {
                    if (c) if (c.charAt(0) == "{" && c.charAt(c.length - 1) == "}") {
                        eval("var result = " + c);
                        if (result.ResultNo != 0) if (result.ResultData && result.ResultData.SecurityResultNo == -99 && _captcha.length == 0) window.location.href = window.location.href;
                        else {
                            $.cookie("resultDescription", result.ResultDescription);
                            window.location.href = $.fost.LoginParams.ReturnUrl;
                            $.Capcha.needCapcha && $.Capcha.updateCapcha();
                            $(".tips-ask").show()
                        } else showTips("\u6709\u672a\u77e5\u9519\u8bef\u53d1\u751f(2)\u3002", true)
                    } else $("#ajaxDisplayer").html(c).show();
                    else showTips("\u6709\u672a\u77e5\u9519\u8bef\u53d1\u751f(1)\u3002", true)
                },
                timeout: 6E4,
                error: function(c, m) {
                    m == "timeout" ? showTips("\u54cd\u5e94\u8d85\u65f6\uff0c\u8bf7\u5237\u65b0\u9875\u9762\u6216\u91cd\u542f\u6d4f\u89c8\u5668\u3002", true) : showTips("\u54cd\u5e94\u8d85\u65f6\uff0c\u8bf7\u5237\u65b0\u9875\u9762\u6216\u91cd\u542f\u6d4f\u89c8\u5668\u3002(" + m + ")", true)
                }
            });
            return false
        }
    };
    $("#txtCode").keyup(function(a) {
        if (a.keyCode != 13) {
            if ($("#txtCode").val().length == 4) {
                if ($.Capcha.CheckValidateCapcha() != true) return false
            } else {
                $("#yanzheng").removeClass("yanzheng_ok");
                $("#yanzheng").removeClass("yanzheng_error")
            }
            b.html("").hide()
        }
    });
    $.fost.security.showBroker(j);
    g.attr("disabled", "");
    h.attr("class", "btnlink_b_small");
    h.click(function() {
        g.parents("form").submit()
    });
    $.Capcha.initCapcha({
        token: $.fost.LoginParams.SecurityToken,
        capchaSrc: "https://passport.5173.com/Sso/RequestCaptcha",
        needCapcha: $.fost.LoginParams.NeedCaptcha
    });
    if ($.fost.LoginParams.AutoDisplayCaptcha) $.Capcha.createCaptcha();
    else {
        var i = document.getElementById("txtName"),
        k = document.getElementById("txtPass");
        document.getElementById("loginSwitcher").checked && $.Capcha.createCaptcha();
        k.value.length > 0 && $.Capcha.createCaptcha();
        i.onfocus = function() {
            k.value.length > 0 && $.Capcha.createCaptcha()
        };
        k.onfocus = function() {
            i.value.length > 0 && $.Capcha.createCaptcha()
        };
        i.onkeydown = function() {
            $.Capcha.createCaptcha()
        };
        i.onclick = function() {
            $.Capcha.createCaptcha()
        }
    }
    j = {
        i: null,
        $passportSlide: $("#passportSlide"),
        $passportUl: $(".passportUl"),
        $focusWrap: $(".focus-wrap"),
        $passportSub: function() {
            return this.$passportUl.find(".passportSub")
        },
        ppsublen: function() {
            return this.$passportSub().length
        },
        ppsubwidth: function() {
            return this.$passportSub().width()
        },
        $passfoucsAr: function() {
            return this.$focusWrap.find("a")
        },
        passfoucsLen: function() {
            return this.$passfoucsAr().length
        },
        focuStr: "",
        firstStr: '<a href="javascript:void(0);" class="active">focus</a>',
        timer: null,
        count: 0,
        init: function() {
            if (this.ppsublen() < 1) this.$passportSlide.css({
                background: "url(https://images002.5173cdn.com/images/login/v1/1m/login_bg7.png) no-repeat"
            });
            else {
                this.setting();
                this.clickSlide();
                this.autoplay()
            }
        },
        setting: function() {
            this.$passportUl.css({
                width: this.ppsublen() * this.ppsubwidth() + "px"
            });
            this.focuStr = this.firstStr + '<a href="javascript:void(0);">focus</a>'.copyStr(this.ppsublen() - 1);
            this.$focusWrap.append(this.focuStr);
            this.$passportSub().show()
        },
        clickSlide: function() {
            var a = this;
            if (a.ppsublen() != 1) for (a.i = 0; a.i < a.passfoucsLen(); a.i++)(function(d) {
                a.$passfoucsAr()[d].onclick = function() {
                    a.count = d;
                    a.tab(a.count)
                }
            })(a.i)
        },
        autoplay: function() {
            var a = this;
            if (a.ppsublen() != 1) {
                clearInterval(a.timer);
                a.timer = setInterval(function() {
                    a.count++;
                    if (a.count == a.passfoucsLen()) a.count = 0;
                    a.tab(a.count)
                },
                3E3);
                a.$passportSlide.hover(function() {
                    clearInterval(a.timer)
                },
                function() {
                    a.timer = setInterval(function() {
                        a.count++;
                        if (a.count == a.passfoucsLen()) a.count = 0;
                        a.tab(a.count)
                    },
                    3E3)
                })
            }
        },
        tab: function(a) {
            this.$passfoucsAr().removeClass("active");
            $(this.$passfoucsAr()[a]).addClass("active");
            this.$passportUl.css({
                left: -a * this.ppsubwidth() + "px"
            })
        }
    };
    String.prototype.copyStr = function(a) {
        var d, e = this.toString(),
        c = "";
        for (d = 0; d < a; d++) if (typeof e == "string") c += e;
        return c
    };
    j.init()
});