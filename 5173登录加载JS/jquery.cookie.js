/*begin*/
/*2012-05-30 18:11:48*/
(function($) {
    $.cookie = function(key, value, options) {
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({},
            options);
            if (value === null || value === undefined) options.expires = -1;
            if (typeof options.expires === "number") {
                var days = options.expires,
                t = options.expires = new Date;
                t.setDate(t.getDate() + days)
            }
            value = String(value);
            return document.cookie = [encodeURIComponent(key), "=", options.raw ? value: encodeURIComponent(value), options.expires ? "; expires=" + options.expires.toUTCString() : "", options.path ? "; path=" + options.path: "", options.domain ? "; domain=" + options.domain: "", options.secure ? "; secure": ""].join("")
        }
        options = value || {};
        var decode = options.raw ?
        function(s) {
            return s
        }: decodeURIComponent;
        var pairs = document.cookie.split("; ");
        for (var i = 0,
        pair; pair = pairs[i] && pairs[i].split("="); i++) try {
            if (decode(pair[0]) === key) {
                var val = decode(pair[1] || "");
                return val
            }
        } catch(e) {}
        return null
    }
})(jQuery);

/*end*/
