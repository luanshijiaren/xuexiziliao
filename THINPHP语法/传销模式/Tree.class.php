<?php

class Tree{
    static public $treeList=array();//存放无限极分类结果
    static public $childNode=array();//存放父节点和父节点下面的子节点

    //无限级分类排序
    public function create($data,$pid = 0,$level = 1){
        foreach($data as $key => $value){
            if($value['l_id']==$pid){
                $value['level'] = $level;
                self::$treeList[] = $value;
                unset($data[$key]);
                self::create($data,$value['id'],$level+1);
            }
        }
        return self::$treeList;
    }
    /******************************************************************************/
    /**
     * 根据父节点，查询父节点下面的子节点
     * @param int $tid  tree表ID
     */
    public function findChild($tid){
        self::$childNode[]=(int)$tid;
        $tree=M('Tree');
        $map['id']=array('eq',$tid);
        $allTree=$tree->field('id,l_id')->select();    //查询tree表
        self::findArrayNode($tid, $allTree);
        return self::$childNode;//返回所有节点
    }

    /**
     *
     * @param int $id   节点ID
     * @param array $list   tree表所有的id,pid
     */
    public function findArrayNode($id,$list){
        foreach ($list as $key => $val){
            if ($id==$val['l_id']){
                self::$childNode[]=(int)$val['id'];
                self::findArrayNode($val['id'], $list);     //递归，传入新节点ID
            }
        }
    }

    /******************************************************************************/

    //根据findChild()的返回结果，组装sql语句,用于查询news表
    public function createSql($arr=array()){
        $sql="";
        foreach ($arr as $key => $val){
            $sql.="tid=$val or ";
        }
        $sql=trim($sql);        //清除空格
        $sql=trim($sql,'or');   //清除最后的or
        $sql=trim($sql);        //清除空格
        return $sql;
    }

    /******************************************************************************/

    /**
     * 根据tid，获取where的sql
     * @param int $tid
     */
    public function getSql($tid){
        return self::createSql(self::findChild($tid));
    }

}