﻿npm -v 查看版本
npm root 查看当前安装包路径
npm root -g全局安装包路径
npm install 安装命令
npm install <全局安装模块> -g  
//npm init      package.json配置
npm ls 显示所有的安装包
npm remove 删除
npm update 更新
npm init 初始化工作环境
npm install --save-dev <需要本地安装的模块>