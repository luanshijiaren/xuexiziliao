module.exports = {
    entry : __dirname + "/src/js/index.js", //唯一入口文件开发环境
    output : {
        path : __dirname + "/assets/js", //生产环境目录用于使用
        filename : "index.js" //生产环境生成文件名
    },
	loaders: [
      {
        test: /\.css$/,  //正则包括css
        loader: 'style-loader!css-loader', //解释器名称
      },
	 plugins:[
		new webpack.HotModuleReplacementPlugin()  //热插拔插件
	 ]
}